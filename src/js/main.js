
$(document).ready(function () {

  ScrollReveal().reveal('#header', { delay: 500 });
  ScrollReveal().reveal('#hero', { delay: 8gulp
      00 });


  $('.seeMoreTextFirst').click(function(e){
    e.preventDefault();
    $('#seeMoreFirstContent').slideToggle(
        ("slow", function () {
              if ($(this).is(':visible'))
                $(this).css('display','inline-flex');
            })
        );
    $(this).text( $(this).text() === 'See More of This.' ? "Show less." : "See More of This.");
  });
  $('.seeMoreTextSecond').click(function(e){
    e.preventDefault();
    $('#seeMoreSecondContent').slideToggle(             //show more and show less with ternary operator
        ("slow", function () {
          if ($(this).is(':visible'))
            $(this).css('display','inline-flex');
        })
    );
    $(this).text( $(this).text() === 'See More of This.' ? "Show less." : "See More of This.");
  });

  $('#header .nav-link, footer .nav-link').click(function() {       //menu navigation to link
    var sectionTo = $(this).attr('href');
    $('html, body').animate({
      scrollTop: $(sectionTo).offset().top
    }, 1500);
  });

});